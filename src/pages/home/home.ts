import { Component, ViewChild } from '@angular/core';
import { NavController, LoadingController, NavParams, AlertController, Slides } from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {KnowldgebasePage} from '../knowldgebase/knowldgebase';
import {GroubsPage} from "../groubs/groubs";
import {PostsPage} from "../posts/posts";
import {MembersPage} from "../members/members";
import {AccountlogPage} from "../accountlog/accountlog";
import {ForumPage} from "../forum/forum";
import {MenuPage} from "../menu/menu";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
  usercookie: any;
  slider: any;
  siteurl: any;
  @ViewChild(Slides) slides: Slides;
  constructor(public translate: TranslateService,public navCtrl: NavController, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController ) {
this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');
    
    let loader = this.loadingController.create({
        content: ""
      });
      loader.present();

      let url = "https://eissaqhalkmp.com/ar/wp-json/wp/v2/pages/984";

    this.http.get(url).map(res => res.json()).subscribe(data => {
        loader.dismiss();
this.slider = data.acf.main_slider
        console.log(this.slider)
      }, (err) => {
        loader.dismiss();
        let alert = this.alertController.create({
          title: 'خطأ',
          subTitle: 'خطا فى الاتصال بالشبكة',
          buttons: ['اغلاق']
        });

        alert.present();
        console.log(err);
      });


    }
    profile(){
      if(this.usercookie == null || this.usercookie == undefined){
        this.navCtrl.push(AccountlogPage);
      }else{
        this.navCtrl.push(MyaccountPage);
      }
    }
    knwldge(){
      this.navCtrl.push(KnowldgebasePage);
    }
    groubs(){
        this.navCtrl.push(GroubsPage);
    }
    posts(){
        this.navCtrl.push(PostsPage);
    }
    members(){
        this.navCtrl.push(MembersPage);
    }
    forums(){
        this.navCtrl.push(ForumPage);
    }
  more(){
    this.navCtrl.push(MenuPage);
  }
}
