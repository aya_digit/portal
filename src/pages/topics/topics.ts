import {Component} from '@angular/core';
import {NavController, LoadingController, NavParams, AlertController} from 'ionic-angular';
import {Headers, Http, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {TopicspostsPage} from "../topicsposts/topicsposts";
import {AccountlogPage} from "../accountlog/accountlog";
import {MenuPage} from "../menu/menu";
import {HomePage} from "../home/home";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';


/**
 * Generated class for the TopicsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-topics',
  templateUrl: 'topics.html',
})
export class TopicsPage {
  furid:any;
  forumss: any;
  mytitle: any;
  usercookie: any;
  myid: any;
  title: any;
  content: any;
  pass: any;
  siteurl: any;
  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');

    this.pass = null;
    this.furid = navParams.get('forumid');
    this.mytitle = navParams.get('forutitle');
    console.log(this.furid)
    let url = this.siteurl + "/api/buddypressread/sitewideforum_get_forum_topics/?forumid=" + this.furid;

    this.http.get(url).map(res => res.json()).subscribe(data => {
      loader.dismiss();
      this.forumss = data[0];
      console.log(  this.forumss);
    }, (err) => {
      loader.dismiss();
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطا فى الاتصال بالشبكة',
        buttons: ['اغلاق']
      });

      alert.present();
      console.log(err);
    });
    console.log(this.usercookie)
if(this.usercookie !== null){
    let url1 = "https://eissaqhalkmp.com/ar/api/user/get_currentuserinfo/?cookie=" + this.usercookie;
    this.http.get(url1).map(res => res.json()).subscribe(data => {

      console.log(data)
      this.myid = data.user.id
    }, (err) => {
      loader.dismiss();
      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });
}
  }
  gotopics(myid){
    this.navCtrl.push( TopicspostsPage, {topid: myid});
  }
  profile(){
    if(this.usercookie == null || this.usercookie == undefined){
      this.navCtrl.push(AccountlogPage);
    }else{
      this.navCtrl.push(MyaccountPage);
    }
  }


  addcomment(){

    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();
    if(this.pass === null){
      let alert = this.alertController.create({
        title: 'كلمة المرور',
        message: "ادخل كلمةالمرور للتحقق من ماهيتك",
        inputs: [
          {
            name: 'password',
            placeholder: 'كلمة المرور'
          },
        ],
        buttons: [
          {
            text: 'اغلاق',
            handler: data => {
              console.log('Cancel clicked');
              loader.dismiss();
            }
          },
          {
            text: 'تاكيد',
            handler: data => {
              console.log('Saved clicked');
              this.pass = data.password;
              loader.dismiss();
            }
          }
        ]
      });
      alert.present();
    }else{

    let url = this.siteurl + "/api/buddypressread/forum_post_topic/";
    let body =  "title=" + this.title + "&content=" + this.content + '&user_id=' + this.myid + '&f_id=' + this.furid + '&pw=' + this.pass;
    console.log(body);
    let headers = new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    });
    let options = new RequestOptions({
      headers: headers,
      body: body,
    });


      this.http.post(url, body, options).map(res => res.json()).subscribe(data => {
        loader.dismiss();
        console.log(data);
        if(data.error == 'Security Error.'){
          let alert = this.alertController.create({
            subTitle: 'كلمة مرور خاطئة',
            buttons: [
              {
                text: 'اغلاق',
                handler: () => {
                  this.navCtrl.pop();
                }
              }
            ]
          });
          alert.present();
        }else{
          let alert = this.alertController.create({
            subTitle: 'عملية ناجحة',
            buttons: [
              {
                text: 'اغلاق',
                handler: () => {
                  this.navCtrl.pop();
                }
              }
            ]
          });
          alert.present();
        }

      }, (err) => {
        loader.dismiss();
        let alert = this.alertController.create({
          title: 'خطأ',
          subTitle: 'خطا فى الاتصال بالشبكة',
          buttons: ['اغلاق']
        });

        alert.present();
        console.log(err);
      });

  }

  }
  home(){
    this.navCtrl.setRoot(HomePage)
  }
  more(){
    this.navCtrl.push(MenuPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForumPage');
  }

}
