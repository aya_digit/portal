import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {AccountlogPage} from "../accountlog/accountlog";
import {MenuPage} from "../menu/menu";
import {HomePage} from "../home/home";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the KnowldgebasesinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-knowldgebasesingle',
  templateUrl: 'knowldgebasesingle.html',
})
export class KnowldgebasesinglePage {
  posttitle: any;
  postid: any;
  postdata: any;
  usercookie: any;
  siteurl: any;
  comments: any;
  namee: any;
  email: any;
  comment: any;
  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    this.postid = this.navParams.get('sinid');
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');

    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();

    let url = this.siteurl + "/wp-json/wp/v2/userpress_wiki/" + this.postid;
    this.http.get(url).map(res => res.json()).subscribe(data => {
      loader.dismiss();
      this.posttitle = data.title.rendered;
      this.postdata = data.content.rendered;
      console.log(data)
    }, (err) => {
      loader.dismiss();
      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });

    let url1 = this.siteurl + "/wp-json/wp/v2/comments?post=" + this.postid;
    this.http.get(url1).map(res => res.json()).subscribe(data => {
      this.comments = data;
      console.log(data)
    }, (err) => {
      loader.dismiss();

      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });


  }

  addcomment() {
    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();
    let url = this.siteurl + "/api/respond/submit_comment/?post_id=" + this.postid;
    let body = "name=" + this.namee + "&email=" + this.email + "&content=" + this.comment;
    console.log(body);
    let headers = new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    });
    let options = new RequestOptions({
      headers: headers,
      body: body,
    });
    if (this.comment == undefined || this.comment == "") {
      loader.dismiss();
      let alert = this.alertController.create({
        subTitle: 'يرجى كتابة تعليق',
        buttons: ['اغلاق']
      });
      alert.present();
    } else if (this.email == undefined || this.email == "") {
      loader.dismiss();
      let alert = this.alertController.create({
        subTitle: 'البريد الالكترونى مطلوب',
        buttons: ['اغلاق']

      });
      alert.present();
    }else if (this.email.indexOf("@") == -1) {
      loader.dismiss();
      let alert = this.alertController.create({
        subTitle: 'البريد الالكترونى خاطئ',
        buttons: ['اغلاق']

      });
      alert.present();
    } else if (this.namee == undefined || this.namee == "") {
      let alert = this.alertController.create({
        subTitle: 'الاسم مطلوب',
        buttons: ['اغلاق']

      });
      alert.present();
    } else {

      this.http.post(url, body, options).map(res => res.json()).subscribe(data => {
        loader.dismiss();
        console.log(data);
        if (data.status == 'pending') {
          let alert = this.alertController.create({
            subTitle: 'جارى مراجعه التعليق',
            buttons: [
              {
                text: 'اغلاق',
              }
            ]
          });
          alert.present();
        }
      }, (err) => {
        loader.dismiss();
        let alert = this.alertController.create({
          title: 'خطأ',
          subTitle: 'خطا فى الاتصال بالشبكة',
          buttons: ['اغلاق']
        });

        alert.present();
        console.log(err);
      });
    }
  }

  profile() {
    if (this.usercookie == null || this.usercookie == undefined) {
      this.navCtrl.push(AccountlogPage);
    } else {
      this.navCtrl.push(MyaccountPage);
    }
  }

home() {
    this.navCtrl.setRoot(HomePage)
  }

more() {
    this.navCtrl.push(MenuPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KnowldgebasesinglePage');
  }

}
