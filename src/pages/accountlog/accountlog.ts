import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {LoginPage} from "../login/login";
import {RegisterPage} from "../register/register";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the AccountlogPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-accountlog',
  templateUrl: 'accountlog.html',
})
export class AccountlogPage {

  constructor(public navCtrl: NavController,public translate: TranslateService, public navParams: NavParams) {
  }

  login(){
    this.navCtrl.push(LoginPage);
  }
  register(){
    this.navCtrl.push(RegisterPage);

  }
}
