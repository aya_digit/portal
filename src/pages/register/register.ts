import {Component} from '@angular/core';
import {NavController, LoadingController, NavParams, AlertController} from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {HomePage} from "../home/home";
import { TranslateService } from '@ngx-translate/core';
import { GeneralService } from '../../providers/general.service';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  username: any;
  nonce: any;
  email: any;
  password: any;
  rpassword: any;
  fname: any;
  lname: any;
  role: any;
  desc: any;

  constructor(public navCtrl: NavController, public generalService:GeneralService,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {


    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();

    let url = "https://eissaqhalkmp.com/ar/api/get_nonce/?controller=user&method=register";
    this.http.get(url).map(res => res.json()).subscribe(data => {

      loader.dismiss();
      console.log(data)
      this.nonce = data.nonce
    }, (err) => {
      loader.dismiss();
      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });


  }

  register() {

    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();

    if (this.password !== this.rpassword) {
      loader.dismiss();
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'كلمتان المرور غير متطابقتان',
        buttons: ['اغلاق']
      });
      alert.present();

    } else {
      let url = "https://eissaqhalkmp.com/ar/api/user/register?nonce=" + this.nonce + "&username=" + this.username + "&user_login=" + this.username + "&email=" + this.email + "&user_pass=" + this.rpassword + "&display_name=" + this.username + "&user_nicename=" + this.username + "&first_name=" + this.fname + "&last_name=" + this.lname + "&role=" + this.role + "&description=" + this.desc;
      this.http.get(url).map(res => res.json()).subscribe(data => {

        if (data.status == 'error') {
          loader.dismiss();
          let alert = this.alertController.create({
            title: 'خطأ',
            subTitle: data.error,
            buttons: ['اغلاق']
          });
          alert.present();
        } else {
          loader.dismiss();
         localStorage.setItem('userc', data.cookie)
         this.generalService.isLoggedIn=true;
          let alert = this.alertController.create({
            title: 'عملية ناجحة',
            subTitle: "تم التسجيل بنجاح",
            buttons: [
              {
                text: 'اغلاق',
                handler: () => {
                  this.navCtrl.setRoot(HomePage);
                }
              }
            ]
          });
          alert.present();

        }
        console.log(data)

      }, (err) => {
        loader.dismiss();
        console.log(err);
        let alert = this.alertController.create({
          title: 'خطأ',
          subTitle: 'خطأ فى الاتصال',
          buttons: ['اغلاق']
        });
        alert.present();
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

}
