import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import {MembersearchresultsPage} from "../membersearchresults/membersearchresults";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the MembersearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-membersearch',
  templateUrl: 'membersearch.html',
})
export class MembersearchPage {
  constructor(public navCtrl: NavController,public translate: TranslateService, public navParams: NavParams) {

  }

  searchdata(imagesearch,countrysearch,gendersearch,interests){
    console.log(imagesearch)
    console.log(countrysearch)
    console.log(gendersearch)
    console.log(interests)
    this.navCtrl.push(MembersearchresultsPage, {imagesearch: imagesearch, countrysearch: countrysearch, gendersearch: gendersearch, interests:interests})
  }

  closeModal() {
    this.navCtrl.pop();
  }
}
