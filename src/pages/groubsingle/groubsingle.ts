import {Component} from '@angular/core';
import {NavController, LoadingController, NavParams, AlertController} from 'ionic-angular';
import {Http, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {AccountlogPage} from "../accountlog/accountlog";
import {MenuPage} from "../menu/menu";
import {HomePage} from "../home/home";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the GroubsinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-groubsingle',
  templateUrl: 'groubsingle.html',
})
export class GroubsinglePage {

    grous: any;
    gid: any;
    groubavater: any;
  groubname: any;
  usercookie: any;
  siteurl: any;
    constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
this.gid = navParams.get('grid');
        let loader = this.loadingController.create({
            content: ""
        });
        loader.present();
      this.usercookie =localStorage.getItem('userc')
      this.siteurl =localStorage.getItem('siteurl');

        let url = this.siteurl + "/api/buddypressread/activity_get_activities/";
        let body = "";
        let headers = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        });
        let options = new RequestOptions({
            headers: headers,
            body: body,
        });
        this.http.post(url, body, options).map(res => res.json()).subscribe(data => {
            loader.dismiss();
            console.log(data);
            this.grous = data.activities;
        }, (err) => {
            loader.dismiss();
            let alert = this.alertController.create({
                title: 'خطأ',
                subTitle: 'خطا فى الاتصال بالشبكة',
                buttons: ['اغلاق']
            });

            alert.present();
            console.log(err);
        });




        let url1 = this.siteurl + "/api/buddypressread/groups_get_groupdetail/?groupId=" + this.gid;
        let body1 = "";
        let headers1 = new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        });
        let options1 = new RequestOptions({
            headers: headers1,
            body: body1,
        });
        this.http.post(url1, body1, options1).map(res => res.json()).subscribe(data => {
            loader.dismiss();
            console.log(data);
            this.groubavater = data.groupfields.avatar;
            this.groubname = data.groupfields.name;
        }, (err) => {
            loader.dismiss();
            let alert = this.alertController.create({
                title: 'خطأ',
                subTitle: 'خطا فى الاتصال بالشبكة',
                buttons: ['اغلاق']
            });

            alert.present();
            console.log(err);
        });







    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GroubsinglePage');
  }
  profile(){
    if(this.usercookie == null || this.usercookie == undefined){
      this.navCtrl.push(AccountlogPage);
    }else{
      this.navCtrl.push(MyaccountPage);
    }
  }
  home(){
    this.navCtrl.setRoot(HomePage)
  }
  more(){
    this.navCtrl.push(MenuPage);
  }
}
