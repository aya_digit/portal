import { Component, ViewChild } from '@angular/core';
import {NavController, NavParams, Slides } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {HomePage} from '../home/home';
import { TranslateService } from '@ngx-translate/core';
/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {
    @ViewChild(Slides) slides: Slides;
    list: any;
    list1: any;
    list2: any;
    constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams) {}
    next() {
        this.slides.slideNext();
    }
    skip() {
        this.navCtrl.setRoot(HomePage);
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad IntroPage');
    }
}
