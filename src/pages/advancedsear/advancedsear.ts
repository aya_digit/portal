import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {PostsinglePage} from "../postsingle/postsingle";
import {AccountlogPage} from "../accountlog/accountlog";
import {MenuPage} from "../menu/menu";
import {HomePage} from "../home/home";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the AdvancedsearPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-advancedsear',
  templateUrl: 'advancedsear.html',
})
export class AdvancedsearPage {
  posts: any;
  keyword: any;
  posttype: any;
  taxonomy: any;
  url: any;
  usercookie: any;
  siteurl: any;

  constructor(public navCtrl: NavController, public http: Http,public translate: TranslateService, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    this.keyword = navParams.get('keyword');
    this.posttype = navParams.get('posttype');
    this.taxonomy = navParams.get('taxonomy');
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');
   
    console.log('mytaxonomy' + this.taxonomy)
    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();
    if (this.posttype == undefined && this.taxonomy == undefined) {

      this.url = this.siteurl + "/api/get_search_results?s=" + this.keyword;

    } else if (this.posttype == undefined) {

      this.url = this.siteurl + "/api/get_search_results?s=" + this.keyword + "&tax_query[field]=tag_ID&tax_query[taxonomy]=userpress_wiki_category&tax_query[terms]=" + this.taxonomy;

    } else if (this.taxonomy == undefined) {

      this.url = this.siteurl + "/api/get_search_results?s=" + this.keyword + "&post_type=" + this.posttype;

    } else {

      this.url = this.siteurl + "/api/get_search_results?s=" + this.keyword + "&post_type=" + this.posttype + "&tax_query[field]=tag_ID&tax_query[taxonomy]=userpress_wiki_category&tax_query[terms]=" + this.taxonomy;

    }
    this.http.get(this.url).map(res => res.json()).subscribe(data => {
      loader.dismiss();
      this.posts = data.posts;
      console.log(data)
    }, (err) => {
      loader.dismiss();

      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });
  }

  gopost(pid) {
    this.navCtrl.push(PostsinglePage, {postid: pid})
  }

  profile() {
    if (this.usercookie == null || this.usercookie == undefined) {
      this.navCtrl.push(AccountlogPage);
    } else {
      this.navCtrl.push(MyaccountPage);
    }
  }

  home() {
    this.navCtrl.setRoot(HomePage)
  }

more() {
    this.navCtrl.push(MenuPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdvancedsearPage');
  }

}
