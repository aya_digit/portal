import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {HomePage} from "../home/home";
import {AdvancedsearchPage} from "../advancedsearch/advancedsearch";
import {AboutusPage} from "../aboutus/aboutus";
import {ContactusPage} from "../contactus/contactus";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  usercookie: any;

  constructor(public navCtrl: NavController,public translate: TranslateService, public navParams: NavParams) {
    this.usercookie =localStorage.getItem('userc')

  }

  home() {
    this.navCtrl.setRoot(HomePage)
  }

  advanced() {
    this.navCtrl.push(AdvancedsearchPage)
  }

  aboutus() {
    this.navCtrl.push(AboutusPage)
  }

  contacus() {
    this.navCtrl.push(ContactusPage)
  }

  en() {
   localStorage.setItem('translate.currentLang', 'en')
   localStorage.setItem('siteurl', 'https://eissaqhalkmp.com/en')
    window.location.reload();
  }

  ar() {
   localStorage.setItem('translate.currentLang', 'ar')
   localStorage.setItem('siteurl', 'https://eissaqhalkmp.com/ar')
    window.location.reload();
  }

  logout() {
   localStorage.removeItem('userc')
    window.location.reload();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

}
