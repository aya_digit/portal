import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {TopicsPage} from "../topics/topics";
import {AccountlogPage} from "../accountlog/accountlog";
import {MenuPage} from "../menu/menu";
import {HomePage} from "../home/home";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ForumPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-forum',
  templateUrl: 'forum.html',
})
export class ForumPage {
  forumss: any;
  usercookie: any;
  siteurl: any;

  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');

    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();
    let url = this.siteurl + "/wp-json/wp/v2/forum?per_page=100";

    this.http.get(url).map(res => res.json()).subscribe(data => {
      loader.dismiss();
      this.forumss = data;
      console.log(this.forumss);

    }, (err) => {
      loader.dismiss();
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطا فى الاتصال بالشبكة',
        buttons: ['اغلاق']
      });

      alert.present();
      console.log(err);
    });


  }

  gotopics(id, title) {
    this.navCtrl.push(TopicsPage, {forumid: id, forutitle: title});
  }

  profile() {
    if (this.usercookie == null || this.usercookie == undefined) {
      this.navCtrl.push(AccountlogPage);
    } else {
      this.navCtrl.push(MyaccountPage);
    }
  }

  home() {
      this.navCtrl.setRoot(HomePage)
  }

more() {
    this.navCtrl.push(MenuPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForumPage');
  }

}
