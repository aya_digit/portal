import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {HomePage} from "../home/home";
import { TranslateService } from '@ngx-translate/core';
import { GeneralService } from '../../providers/general.service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username: any;
  password: any;

  constructor(public navCtrl: NavController, public generalService:GeneralService,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {

  }

  login() {

    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();

    let url = "https://eissaqhalkmp.com/ar/api/user/generate_auth_cookie?username=" + this.username + "&password=" + this.password;
    this.http.get(url).map(res => res.json()).subscribe(data => {

      if (data.status == 'error') {
        loader.dismiss();
        let alert = this.alertController.create({
          title: 'خطأ',
          subTitle: data.error,
          buttons: ['اغلاق']
        });
        alert.present();
      } else {
        loader.dismiss();
       localStorage.setItem('userc', data.cookie)
       this.generalService.isLoggedIn=true;
        let alert = this.alertController.create({
          title: 'عملية ناجحة',
          subTitle: "مرحبا بك" + data.user.username,
          buttons: [
            {
              text: 'اغلاق',
              handler: () => {
                this.navCtrl.setRoot(HomePage);
              }
            }
          ]
        });
        alert.present();

      }
      console.log(data)

    }, (err) => {
      loader.dismiss();
      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
