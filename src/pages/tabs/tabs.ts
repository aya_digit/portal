import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { MenuPage } from '../menu/menu';
import { AdvancedsearchPage } from '../advancedsearch/advancedsearch';
import { TranslateService } from '@ngx-translate/core';
import { AccountlogPage } from '../accountlog/accountlog';
import { MenuController } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = AccountlogPage;
  tab2Root: any = HomePage;
  tab3Root: any = MenuPage;

  constructor(public translate: TranslateService,public menuCtrl:MenuController) {
    // if(this.usercookie == null || this.usercookie == undefined){
    //   this.navCtrl.push(AccountlogPage);
    // }else{
    //   this.navCtrl.push(MyaccountPage);
    // }
  }
  openSideMenu(){
    this.menuCtrl.toggle();
    // if(this.translate.currentLang == "en"){
    //   this.menuCtrl.toggle("left")
    // }else{
    //   this.menuCtrl.toggle("right")      
    // }
    
    }
}
