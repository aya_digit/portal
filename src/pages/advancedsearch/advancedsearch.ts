import { Component } from '@angular/core';
import {  NavController, LoadingController, NavParams, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {AdvancedsearPage} from "../advancedsear/advancedsear";
import {AccountlogPage} from "../accountlog/accountlog";
import {MenuPage} from "../menu/menu";
import {HomePage} from "../home/home";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the AdvancedsearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-advancedsearch',
  templateUrl: 'advancedsearch.html',
})
export class AdvancedsearchPage {

  cattitle: any;
  usercookie: any;
  siteurl: any;
  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');
    let url =  this.siteurl + "/wp-json/wp/v2/userpress_wiki_category?per_page=100";
    this.http.get(url).map(res => res.json()).subscribe(data => {
      loader.dismiss();
      this.cattitle = data;
      console.log(data)
    }, (err) => {
      loader.dismiss();

      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });

  }
  searchdata(search1,search2,search3){
    console.log(search1)
    console.log(search2)
    console.log(search3)
    this.navCtrl.push(AdvancedsearPage, {keyword: search1, posttype: search2, taxonomy: search3})
  }
  profile(){
    if(this.usercookie == null || this.usercookie == undefined){
      this.navCtrl.push(AccountlogPage);
    }else{
      this.navCtrl.push(MyaccountPage);
    }
  }
  home(){
      this.navCtrl.setRoot(HomePage)
  }
  more(){
    this.navCtrl.push(MenuPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AdvancedsearchPage');
  }

}
