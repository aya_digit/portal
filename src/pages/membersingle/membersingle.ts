import {Component} from '@angular/core';
import {NavController, LoadingController, NavParams, AlertController} from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the MembersinglePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-membersingle',
  templateUrl: 'membersingle.html',
})
export class MembersinglePage {
  username: any;
  email: any;
  fname: any;
  lname: any;
  desc: any;
  user_id: any;
  avatar: any;
  role: any;
  gender: any;
  country: any;
  phonenumber: any;
  interests: any;
  siteurl: any;
  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    this.user_id = navParams.get('memid');
console.log(this.user_id)
    this.siteurl =localStorage.getItem('siteurl');

    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();

    let url = this.siteurl + "/api/user/get_userinfo/?user_id=" + this.user_id;
    this.http.get(url).map(res => res.json()).subscribe(data => {

      loader.dismiss();
      console.log(data)
      this.username = data.username
      this.email = data.email
      this.fname = data.firstname
      this.lname = data.lastname
      this.avatar = data.avatar
      this.role = data.role
      this.gender = data.gender
      this.country = data.country
      this.phonenumber = data.phone
      this.interests = data.interests
    }, (err) => {
      loader.dismiss();
      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });


  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MyaccountPage');
  }

}
