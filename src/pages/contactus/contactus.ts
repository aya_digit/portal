import {Component} from '@angular/core';
import {NavController, LoadingController, NavParams, AlertController} from 'ionic-angular';
import {Http, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the ContactusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-contactus',
  templateUrl: 'contactus.html',
})
export class ContactusPage {
  namee: any;
  phonee: any;
  email: any;
  message: any;
  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {


  }
  addcomment(){

    if(this.namee == undefined || this.namee == ""){
      let alert = this.alertController.create({
        subTitle: 'الاسم مطلوب',
        buttons: ['اغلاق']
      });
      alert.present();
    }else if(this.email == undefined || this.email == ""){
      let alert = this.alertController.create({
        subTitle: 'البريد الالكترونى مطلوب',
        buttons: ['اغلاق']

      });
      alert.present();
    }else if (this.email.indexOf("@") == -1) {
      let alert = this.alertController.create({
        subTitle: 'البريد الالكترونى خاطئ',
        buttons: ['اغلاق']

      });
      alert.present();
    } else {


    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactusPage');
  }

}
