import {Component} from '@angular/core';
import {NavController, LoadingController, NavParams, AlertController} from 'ionic-angular';
import {Http, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {AccountlogPage} from "../accountlog/accountlog";
import {MenuPage} from "../menu/menu";
import {HomePage} from "../home/home";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the TopicspostsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-topicsposts',
  templateUrl: 'topicsposts.html',
})

export class TopicspostsPage {

  toid: any;
  dataa: any;
  pcount: any;
  topicname: any;
  forumname: any;
  email: any;
  comment : any;
  usercookie : any;
  siteurl: any;
  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
this.toid = navParams.get('topid');
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');

console.log(this.toid);
    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();
    let url = this.siteurl + "/api/buddypressread/groupforum_get_topic_posts?topicid=" + this.toid;

    this.http.get(url).map(res => res.json()).subscribe(data => {
      loader.dismiss();
      this.topicname = data.topic.title;
      this.forumname = data.topic.forum_name;
      this.pcount = data.count;
      this.dataa = data.results;
      console.log(this.dataa);


    }, (err) => {
      loader.dismiss();
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطا فى الاتصال بالشبكة',
        buttons: ['اغلاق']
      });

      alert.present();
      console.log(err);
    });


  }

  addcomment(){

    let loader = this.loadingController.create({
      content: ""
    });
loader.present();
    let url = this.siteurl + "/wp-json/bbp-api/v1/topics/" + this.toid;
    let body =  "email=" + this.email + "&content=" + this.comment;
    console.log(body);
    let headers = new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    });
    let options = new RequestOptions({
      headers: headers,
      body: body,
    });
    if(this.comment == undefined || this.comment == ""){
      loader.dismiss();
      let alert = this.alertController.create({
        subTitle: 'يرجى كتابة رد',
        buttons: ['اغلاق']
      });
      alert.present();
    }else if(this.email == undefined || this.email == ""){
      loader.dismiss();
      let alert = this.alertController.create({
        subTitle: 'البريد الالكترونى مطلوب',
        buttons: ['اغلاق']

      });
      alert.present();
    }else if (this.email.indexOf("@") == -1) {
      loader.dismiss();
      let alert = this.alertController.create({
        subTitle: 'البريد الالكترونى خاطئ',
        buttons: ['اغلاق']

      });
      alert.present();
    } else {

      this.http.post(url, body, options).map(res => res.json()).subscribe(data => {
        loader.dismiss();
        console.log(data);
          let alert = this.alertController.create({
            subTitle: 'عملية ناجحة',
            buttons: [
              {
                text: 'اغلاق',
                handler: () => {
                  this.navCtrl.pop();
                }
              }
            ]
          });
          alert.present();
      }, (err) => {
        loader.dismiss();
        let alert = this.alertController.create({
          title: 'خطأ',
          subTitle: 'خطا فى الاتصال بالشبكة',
          buttons: ['اغلاق']
        });

        alert.present();
        console.log(err);
      });
    }
  }
  profile(){
    if(this.usercookie == null || this.usercookie == undefined){
      this.navCtrl.push(AccountlogPage);
    }else{
      this.navCtrl.push(MyaccountPage);
    }
  }
  home(){
    this.navCtrl.setRoot(HomePage)
  }
  more(){
    this.navCtrl.push(MenuPage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForumPage');
  }

}
