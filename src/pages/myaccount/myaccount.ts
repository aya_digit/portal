import {Component} from '@angular/core';
import {NavController, LoadingController, NavParams, AlertController} from 'ionic-angular';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the MyaccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html',
})
export class MyaccountPage {

  usercookie: any;
  username: any;
  email: any;
  fname: any;
  lname: any;
  desc: any;
  role: any;
  roles: any;
  gender: any;
  country: any;
  phonenumber: any;
  interests: any;
  siteurl: any;
  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');
    console.log(this.usercookie)

    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();

    let url = "https://eissaqhalkmp.com/ar/api/user/get_currentuserinfo/?cookie=" + this.usercookie;
    this.http.get(url).map(res => res.json()).subscribe(data => {

      loader.dismiss();
      console.log(data)
      this.role = data.user.role
      this.roles = data.user.roles
      this.username = data.user.username
      this.email = data.user.email
      this.fname = data.user.firstname
      this.lname = data.user.lastname
      this.desc = data.user.description
      this.gender = data.user.gender
      this.country = data.user.country
      this.phonenumber = data.user.phone
      this.interests = data.user.interests
    }, (err) => {
      loader.dismiss();
      console.log(err);
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطأ فى الاتصال',
        buttons: ['اغلاق']
      });
      alert.present();
    });


  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MyaccountPage');
  }

}
