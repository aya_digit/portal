import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, NavParams} from 'ionic-angular';
import {Headers, Http, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {GroubsinglePage} from "../groubsingle/groubsingle";
import {AccountlogPage} from "../accountlog/accountlog";
import {HomePage} from "../home/home";
import {MenuPage} from "../menu/menu";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';

/**
 * Generated class for the GroubsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-groubs',
  templateUrl: 'groubs.html',
})
export class GroubsPage {
  groubs: any;
  usercookie: any;
  siteurl: any;

  constructor(public navCtrl: NavController,public translate: TranslateService, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');

    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();
    let url = this.siteurl + "/api/buddypressread/groups_get_groups/";
    let body = "";
    let headers = new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    });
    let options = new RequestOptions({
      headers: headers,
      body: body,
    });
    this.http.post(url, body, options).map(res => res.json()).subscribe(data => {
      loader.dismiss();
      console.log(data);
      this.groubs = data.groups;
    }, (err) => {
      loader.dismiss();
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطا فى الاتصال بالشبكة',
        buttons: ['اغلاق']
      });

      alert.present();
      console.log(err);
    });

  }

  gogroub(gid) {
    this.navCtrl.push(GroubsinglePage, {grid: gid})
  }

  profile() {
    if (this.usercookie == null || this.usercookie == undefined) {
      this.navCtrl.push(AccountlogPage);
    } else {
      this.navCtrl.push(MyaccountPage);
    }
  }

home() {
    this.navCtrl.setRoot(HomePage)
  }

more() {
    this.navCtrl.push(MenuPage);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GroubsPage');
  }

}
