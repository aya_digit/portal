import {Component} from '@angular/core';
import {NavController, LoadingController, NavParams, AlertController, ModalController} from 'ionic-angular';
import {Http, Headers, RequestOptions} from "@angular/http";
import 'rxjs/add/operator/map';
import {MembersinglePage} from "../membersingle/membersingle";
import {AccountlogPage} from "../accountlog/accountlog";
import {HomePage} from "../home/home";
import {MenuPage} from "../menu/menu";
import {MembersearchPage} from "../membersearch/membersearch";
import {MyaccountPage} from "../myaccount/myaccount";
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'page-membersearchresults',
  templateUrl: 'membersearchresults.html',
})
export class MembersearchresultsPage {
  members: any;
  imagesearch: any;
  countrysearch: any;
  gendersearch: any;
  interests: any;
  mygender: any;
  usercookie: any;
  siteurl: any;
  constructor(public navCtrl: NavController,public translate: TranslateService, public modalCtrl: ModalController, public http: Http, public navParams: NavParams, public loadingController: LoadingController, private alertController: AlertController) {
    this.imagesearch = this.navParams.get('imagesearch');
    this.countrysearch = this.navParams.get('countrysearch');
    this.gendersearch = this.navParams.get('gendersearch');
    this.interests = this.navParams.get('interests');
    this.usercookie =localStorage.getItem('userc')
    this.siteurl =localStorage.getItem('siteurl');

    console.log('image' + this.imagesearch + 'countru' + this.countrysearch + 'gender' + this.gendersearch + 'inter' + this.interests)
    let loader = this.loadingController.create({
      content: ""
    });
    loader.present();

    let url = this.siteurl + "/api/buddypressread/userssearch/?gender=" + this.gendersearch + "&country=" + this.countrysearch + "&interst=" + this.interests;
    let body = "";
    let headers = new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    });
    let options = new RequestOptions({
      headers: headers,
      body: body,
    });
    this.http.post(url, body, options).map(res => res.json()).subscribe(data => {
      loader.dismiss();
  this.members = data[0];

    }, (err) => {
      loader.dismiss();
      let alert = this.alertController.create({
        title: 'خطأ',
        subTitle: 'خطا فى الاتصال بالشبكة',
        buttons: ['اغلاق']
      });

      alert.present();
      console.log(err);
    });

  }
  profile(){
    if(this.usercookie == null || this.usercookie == undefined){
      this.navCtrl.push(AccountlogPage);
    }else{
      this.navCtrl.push(MyaccountPage);
    }
  }
home() {
    this.navCtrl.setRoot(HomePage)
  }

more() {
    this.navCtrl.push(MenuPage);
  }
  member(id){
    this.navCtrl.push(MembersinglePage, {memid: id})
  }
  openModal() {
    let myModal = this.modalCtrl.create(MembersearchPage);
    myModal.present();
  }

}
