import { NgModule } from '@angular/core';
import { IonicApp, IonicModule, NavController } from 'ionic-angular';
import { MyApp } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http, HttpModule } from '@angular/http';
import { LanguageService } from '../providers/language.service';
import { SplashScreen} from "@ionic-native/splash-screen";
import { StatusBar} from "@ionic-native/status-bar";
import { KnowldgebasePage } from '../pages/knowldgebase/knowldgebase';
import { KnowldgebaseallPage } from '../pages/knowldgebaseall/knowldgebaseall';
import { KnowldgebasesinglePage } from '../pages/knowldgebasesingle/knowldgebasesingle';
import { GroubsPage } from '../pages/groubs/groubs';
import { GroubsinglePage } from '../pages/groubsingle/groubsingle';
import { MembersPage } from '../pages/members/members';
import { MembersinglePage } from '../pages/membersingle/membersingle';
import { PostsPage } from '../pages/posts/posts';
import { PostsinglePage } from '../pages/postsingle/postsingle';
import { RegisterPage } from '../pages/register/register';
import { LoginPage } from '../pages/login/login';
import { AccountlogPage } from '../pages/accountlog/accountlog';
import { IntroPage } from '../pages/intro/intro';
import { ForumPage } from '../pages/forum/forum';
import { TopicsPage } from '../pages/topics/topics';
import { MenuPage } from '../pages/menu/menu';
import { AdvancedsearchPage } from '../pages/advancedsearch/advancedsearch';
import { TopicspostsPage } from '../pages/topicsposts/topicsposts';
import { AdvancedsearPage } from '../pages/advancedsear/advancedsear';
import { AboutusPage } from '../pages/aboutus/aboutus';
import { ContactusPage } from '../pages/contactus/contactus';
import { MembersearchPage } from '../pages/membersearch/membersearch';
import { MembersearchresultsPage } from '../pages/membersearchresults/membersearchresults';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { GeneralService } from '../providers/general.service';

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    KnowldgebasePage,
    KnowldgebaseallPage,
    KnowldgebasesinglePage,
    GroubsPage,
    GroubsinglePage,
    MembersPage,
    MembersinglePage,
    PostsPage,
    PostsinglePage,
    RegisterPage,
    LoginPage,
    AccountlogPage,
    IntroPage,
    ForumPage,
    TopicsPage,
    TopicspostsPage,
    MenuPage,
    AdvancedsearchPage,
    AdvancedsearPage,
    AboutusPage,
    ContactusPage,
    MembersearchPage,
    MembersearchresultsPage,
    MyaccountPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    KnowldgebasePage,
    KnowldgebaseallPage,
    KnowldgebasesinglePage,
    GroubsPage,
    GroubsinglePage,
    MembersPage,
    MembersinglePage,
    PostsPage,
    PostsinglePage,
    RegisterPage,
    LoginPage,
    AccountlogPage,
    IntroPage,
    ForumPage,
    TopicsPage,
    TopicspostsPage,
    MenuPage,
    AdvancedsearchPage,
    AdvancedsearPage,
    AboutusPage,
    ContactusPage,
    MembersearchPage,
    MembersearchresultsPage,
    MyaccountPage
  ],
  providers: [
    LanguageService,
    SplashScreen,
    StatusBar,
    GeneralService]
})
export class AppModule {}
