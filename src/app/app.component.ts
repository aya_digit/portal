import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { TabsPage } from '../pages/tabs/tabs';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { HomePage } from '../pages/home/home';
import { AdvancedsearchPage } from '../pages/advancedsearch/advancedsearch';
import { AboutusPage } from '../pages/aboutus/aboutus';
import { ContactusPage } from '../pages/contactus/contactus';
import { GeneralService } from '../providers/general.service';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav; 
  rootPage = TabsPage;
  textDir: string = "rtl";
  uid: any;
  siteurl: any;
  pages: any[] = [
    { title: 'default', pageName: HomePage, icon: 'home' },
    { title: 'Advancedsearch', pageName: AdvancedsearchPage, icon: 'search' },
    { title: 'Contactus', pageName: ContactusPage, icon: 'call' },
    { title: 'Aboutus', pageName: AboutusPage, icon: 'information-circle' }
    // { title: 'en', pageName: HomePage, icon: 'globe' },
    // { title: 'عربي', pageName: HomePage, icon: 'globe' },
    // { title: 'Logout', pageName: HomePage, icon: 'log-out' }

  ];
 
  constructor(
    platform: Platform,
    public generalService:GeneralService,
    public translate: TranslateService,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen) {

      this.uid = 'aa123432';
    translate.setDefaultLang('ar');
    translate.use('ar');
    
    platform.ready().then(() => {
      localStorage.setItem('deid', this.uid)
      this.siteurl =localStorage.getItem('siteurl');
      if (!this.siteurl) {
      localStorage.setItem('siteurl', 'https://eissaqhalkmp.com/ar')
      }
      if(localStorage.getItem('userc')){
      this.generalService.isLoggedIn=true;}
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //this is to determine the text direction depending on the selected language
      this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
        // translate.setDefaultLang(event.lang);
        this.textDir = translate.currentLang == 'ar' ? 'rtl' : 'ltr';
      //  this.menu.enable(true)
      });
    });

  }
  lang(element: string) {
    this.translate.setDefaultLang(element); this.translate.use(element);
 //   this.textDir = this.translate.currentLang == 'ar' ? 'rtl' : 'ltr';
 }
  openPage(p) {
    if (p.pageName) {
      if(p.pageName==HomePage)
      {
        this.nav.setRoot(TabsPage);
      }else{
      this.nav.push(p.pageName);
      }
    }
  }
  logout() {
    localStorage.removeItem('userc');
  this.generalService.isLoggedIn=false;
  }

}
