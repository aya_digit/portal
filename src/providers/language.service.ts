import { Injectable } from "@angular/core";
import { LanguageModel } from "../models/language.model";

@Injectable()
export class LanguageService {
  languages : Array<LanguageModel> = new Array<LanguageModel>();

   constructor() {
     this.languages.push(
       {name: "en", code: "en"},
       {name: "Spanish", code: "es"},
       {name: "ar", code: "ar"}
     );
   }

   getLanguages(){
     return this.languages;
   }
 }
